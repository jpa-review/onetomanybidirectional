package com.twuc.webApp.entities;

import com.twuc.webApp.repositories.StudentRepository;
import com.twuc.webApp.repositories.TeacherRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest(showSql = false)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class OneToManyBidirectionalTest {
    @Autowired
    TeacherRepository teacherRepository;

    @Autowired
    StudentRepository studentRepository;

    @Autowired
    EntityManager entityManager;

    @Test
    void should_save_by_bidirectional() {
        Teacher teacher = new Teacher("LiJiahao");
        Student student = new Student("Eason");
        Student anotherStudent = new Student("JJ");

        teacherRepository.saveAndFlush(teacher);

        teacher.addStudent(student);
        teacher.addStudent(anotherStudent);

        entityManager.flush();
        entityManager.clear();

        int teachersNumber = teacherRepository.findAll().size();
        int studentNumber = studentRepository.findAll().size();

        assertEquals(1, teachersNumber);
        assertEquals(2, studentNumber);
    }
}